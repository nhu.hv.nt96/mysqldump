FROM golang:1.17-buster as mod
WORKDIR /src
COPY go.mod go.sum ./
RUN CGO_ENABLED=1 go mod download

FROM golang:1.17-buster as build
WORKDIR /src
COPY --from=mod /go/pkg/mod /go/pkg/mod
COPY . .
#RUN CGO_ENABLED=1 GOOS=linux GOARCH=amd64 go build -o app cmd/app/main.go
RUN GOOS=linux GOARCH=amd64 go build -o app main.go


FROM golang:1.17-buster as relese
WORKDIR /event
#RUN mkdir images
COPY --from=build /src/app ./app

CMD cd /event \
    && ./app