package source

import (
	"fmt"
	"os"

	"app/mysqldump"
)

func Source(dns, env, path, database, table string) error {
	f, err := os.Open(path + "/" + database + "/" + table + ".sql")
	if err != nil {
		return err
	}

	return mysqldump.Source(
		fmt.Sprintf(dns, env, database+"_dump"),
		f,
		mysqldump.WithMergeInsert(100),
		//mysqldump.WithDryRun(),
		mysqldump.WithDebug(),
	)
}
