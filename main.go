package main

import (
	"app/dump"
	"app/source"
	"app/utils"
	"database/sql"
	"fmt"
	"github.com/robfig/cron/v3"
	"log"
	"os"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

type Company struct {
	Name   string `json:"name"`
	Code   string `json:"code"`
	Status int    `json:"status"`
}

var (
	tables = []string{
		"allocation_rate",
		"comment",
		"cost_reports",
		"department",
		"pond",
		"province",
		"district",
		"fcm",
		"filter_fish",
		"food",
		"general_cost",
		"group",
		"group_permission",
		"group_user",
		"head_department",
		"interest_rate",
		"inventory_daily_report",
		"inventory_report",
		"inventory_stock",
		"inventory_summary",
		"inventory_transaction",
		"manage_department",
		"medicine",
		"medicine_purchase",
		"migrate",
		"notification",
		"notification_read",
		"order_business",
		"order_detail",
		"order_detail_log",
		"otp",
		"payment_voucher",
		"permission",
		"region",
		"purchase",
		"purchase_detail",
		"re_assignees",
		"reaction",
		"role",
		"season",
		"season_detail",
		"season_report",
		"season_summary",
		"summary",
		"supplier",
		"supplier_product",
		"workflow",
		"workflow_data",
		"task",
		"task_assignees",
		"upload",
		"user",
		"user_department",
		"user_role",
		"warehouse",
		"warehouse_department",
	}
	dbPrefixName = "pms_prod_"
	dns          = "root:Dst@123@tcp(%s)/%s?charset=utf8mb4&parseTime=True&loc=Local"
	envProd      = "27.71.234.62:3306"
	envDev       = "172.17.0.2:3306"
)

func main() {
	utc, _ := time.LoadLocation("Asia/Ho_Chi_Minh")
	time.Local = utc
	cron := cron.New()
	cron.AddFunc("40 00 * * *", script) // every day at 00:40
	cron.Start()
	fmt.Println("Cron start")
	var forever chan struct{}
	<-forever
}

func script() {
	t := time.Now()
	path := fmt.Sprintf("db/%d.%d.%d", t.Day(), t.Month(), t.Year())
	if _, err := os.Stat(path); os.IsNotExist(err) {
		if err := os.Mkdir(path, 0777); err != nil {
			log.Fatal(err)
		}
	}

	db, err := sql.Open("mysql", fmt.Sprintf(dns, envProd, dbPrefixName+"master"))
	if err != nil {
		log.Printf("[error] %v \n", err)
		return
	}
	defer db.Close()

	rows, err := db.Query("SELECT name, code, status FROM company WHERE status = 1")
	if err != nil {
		log.Printf("[error] %v \n", err)
		return
	}
	var companies []Company
	for rows.Next() {
		var company Company
		if err := rows.Scan(&company.Name, &company.Code, &company.Status); err != nil {
			log.Println(err.Error())
		}
		companies = append(companies, company)
	}

	for _, company := range companies {
		var (
			ok   bool = true
			text string
		)
		for _, table := range tables {
			err := dump.Dump(dns, envProd, path, dbPrefixName+company.Code, table)
			if err != nil {
				log.Printf("[error] %v \n", err)
				ok = false
				break
			}
		}
		if ok {
			text = fmt.Sprintf("%s - %s - %s =====> OK", time.Now().Format("02/01/2006"), company.Name, "dump")
		} else {
			text = fmt.Sprintf("%s - %s - %s =====> FAILED", time.Now().Format("02/01/2006"), company.Name, "dump")
		}
		utils.TelegramChatBox(text)
	}

	for _, company := range companies {
		var (
			ok   bool = true
			text string
		)
		// env backup
		if err := _source(dns, envDev, path, dbPrefixName+company.Code); err != nil {
			ok = false
		}
		if ok {
			text = fmt.Sprintf("%s - %s - %s =====> OK", time.Now().Format("02/01/2006"), company.Name, "migrate")
		} else {
			text = fmt.Sprintf("%s - %s - %s =====> FAILED", time.Now().Format("02/01/2006"), company.Name, "migrate")
		}
		utils.TelegramChatBox(text)
	}
}

func _source(dns, env, path, dbs string) error {
	db, err := sql.Open("mysql", fmt.Sprintf(dns, env, "pms_dev_baomi"))
	if err != nil {
		log.Printf("[error] %v \n", err)
		return err
	}
	defer db.Close()
	_, err = db.Exec(fmt.Sprintf("DROP DATABASE IF EXISTS %s", dbs+"_dump"))
	if err != nil {
		log.Printf("[error] %v \n", err)
		return err
	}
	_, err = db.Exec(fmt.Sprintf("CREATE DATABASE IF NOT EXISTS %s", dbs+"_dump"))
	if err != nil {
		log.Printf("[error] %v \n", err)
		return err
	}

	for _, table := range tables {
		err := source.Source(dns, env, path, dbs, table)
		if err != nil {
			log.Printf("[error] %v \n", err)
			return err
		}
	}
	return nil
}
