package dump

import (
	"fmt"
	"os"

	"app/mysqldump"
)

func Dump(dns, env, path, database, table string) error {
	if _, err := os.Stat(path + "/" + database); os.IsNotExist(err) {
		if err := os.Mkdir(path+"/"+database, 0777); err != nil {
			return err
		}
	}
	f, err := os.Create(path + "/" + database + "/" + table + ".sql")
	if err != nil {
		return err
	}

	return mysqldump.Dump(
		fmt.Sprintf(dns, env, database), // DNS
		//mysqldump.WithDropTable(),   // Option: Delete table before create (Default: Not delete table)
		mysqldump.WithData(),        // Option: Dump Data (Default: Only dump table schema)
		mysqldump.WithTables(table), // Option: Dump Tables (Default: All tables)
		mysqldump.WithWriter(f),     // Option: Writer (Default: os.Stdout)
	)
}
