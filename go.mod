module app

go 1.18

require (
	github.com/go-sql-driver/mysql v1.7.0
	github.com/jarvanstack/mysqldump v0.7.0
)

require (
	github.com/robfig/cron/v3 v3.0.1 // indirect
	github.com/traywzk/mysqldump v1.0.3 // indirect
)
